FROM node:current
WORKDIR /usr/src/app
EXPOSE 4000
COPY . /usr/src/app/
RUN npm install
RUN npm run build
CMD ["npm", "start"]	
