import Pagination from '../../src/lib/Pagination/Pagination'
import IPagData from '../../src/lib/Pagination/IPagData'
import IPagination from '../../src/lib/Pagination/IPagination'
// todo: доделать тест
const targetArray: Array<number> = [1, 2, 3, 4, 5, 6, 7, 8, 9]

it('pagination', async () => {
  let page: number = 1
  let perPage: number = 2

  const paginationResult: IPagData<number> = Pagination.pagination(
    page,
    perPage,
    targetArray
  )
  console.log(paginationResult)
  expect(paginationResult.data.length).toEqual(2)
})
