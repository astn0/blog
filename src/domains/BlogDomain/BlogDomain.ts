import BlogProxy from '../../proxy/BlogProxy/BlogProxy'
import Ipost from '../../proxy/BlogProxy/IPost'
import Pagination from '../../lib/Pagination/Pagination'
import IPagData from '../../lib/Pagination/IPagData'
import IPostString from '../../proxy/BlogProxy/IPostString'
import IPostNumber from '../../proxy/BlogProxy/IPostNumber'

export default class BlogDomain {
  /**
   * Создание нового поста
   */
  public static async createOnePost(newPost: Ipost): Promise<Ipost> {
    console.log('domain : createOnePost')
    const blogProxy = new BlogProxy()
    const resultPost: Ipost = await blogProxy.createOnePost(newPost)
    return resultPost
  }

  /**
   * Получение всех постов
   */
  public static async getAllPosts(
    author: IPostString,
    tag: IPostString,
    page: IPostNumber,
    perPage: IPostNumber
  ): Promise<IPagData<Ipost>> {
    const blogProxy = new BlogProxy()
    const resultPosts: Array<Ipost> = await blogProxy.getAllPosts()

    // todo: create Author filter

    return Pagination.pagination(page as number, perPage as number, resultPosts)
  }

  /**
   * Получение заданного поста
   *
   * @param postID
   */
  public static async getOnePost(postID: string): Promise<Ipost> {
    const blogProxy = new BlogProxy()
    const resultPost: Ipost = await blogProxy.getOnePost(postID)
    return resultPost
  }
  /**
   * Внесение изменений в заданный пост
   *
   * @param newPost
   */
  public static async patchOnePost(newPost: Ipost): Promise<Ipost> {
    const blogProxy = new BlogProxy()
    const resultPost: Ipost = await blogProxy.patchOnePost(newPost)
    return resultPost
  }
}
