import { DataTypes, Model } from 'sequelize'
import SequelizeSingleton from '../../lib/SequelizeSinglton'
/**
 * Модель - связка между моделью болгов и моделью тегов
 */
class TagsBlogModel extends Model {}

const sequelize = SequelizeSingleton
TagsBlogModel.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    postID: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    tagID: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

},
{
    sequelize, 
    modelName: 'TagsBlog',
    tableName: 'tags_blog'
})

export default TagsBlogModel