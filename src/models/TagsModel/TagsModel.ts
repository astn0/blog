import { DataTypes, Model } from 'sequelize'
import SequelizeSingleton from '../../lib/SequelizeSinglton'

/**
 * Модель тегов
 */
class TagsModel extends Model { }

// const sequelize = SequelizeSingleton
TagsModel.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    }

},
{
    sequelize: SequelizeSingleton, 
    modelName: 'Tags',
    tableName: 'tags'
})

TagsModel.sync()
export default TagsModel