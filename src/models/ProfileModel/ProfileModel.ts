import { DataTypes, Model } from 'sequelize'
import SequelizeSingleton from '../../lib/SequelizeSinglton'

class ProfileModel extends Model { }

// const sequelize = SequelizeSingleton
ProfileModel.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    login: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phone: {
        type: DataTypes.STRING,
        allowNull: false
    },
    avatar: {
        type: DataTypes.STRING,
        allowNull: true
    }

},
{
    sequelize: SequelizeSingleton, 
    modelName: 'Profile',
    tableName: 'profile'
})
ProfileModel.sync()
export default ProfileModel 
