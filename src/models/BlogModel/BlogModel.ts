import { DataTypes, Model } from 'sequelize'
import SequelizeSingleton from '../../lib/SequelizeSinglton'

/**
 * Модель для блога
 */
class BlogModel extends Model { }

// const sequelize = SequelizeSingleton
BlogModel.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    header: {
        type: DataTypes.STRING,
        allowNull: false
    },
    preview: {
        type: DataTypes.STRING,
        allowNull: true
    },
    text: {
        type: DataTypes.STRING,
        allowNull: true
    },
    author: {
        type: DataTypes.STRING,
        allowNull: false
    },
    img: {
        type: DataTypes.STRING,
        allowNull: true
    },
    status: {
        type: DataTypes.STRING,
        allowNull: false
    },
    isDeleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    }
},
{
    sequelize: SequelizeSingleton, 
    modelName: 'Blog',
    tableName: 'blog'
})
BlogModel.sync()
export default BlogModel 
