import { Request, Response, NextFunction } from 'express'
import { check, validationResult } from 'express-validator'
import BlogDomain from '../domains/BlogDomain/BlogDomain'
import Ipost from '../proxy/BlogProxy/IPost'
import PostStatus from '../proxy/BlogProxy/PostStatus'
import IPagData from '../lib/Pagination/IPagData'
import IPostString from '../proxy/BlogProxy/IPostString'
import ITag from '../proxy/BlogProxy/ITag'
import IPostNumber from '../proxy/BlogProxy/IPostNumber'

export default class BlogController {
  // todo: добавь deleted и visible
  // todo: добавь delete post = _ => post.deleted = true
  /**
   * Поиск нужных постов постов
   *
   * @param req
   * @param res
   */
  public static async searchPosts(req: Request, res: Response): Promise<void> {
    // todo: добавь картинку поста
    // todo: добавь фильтр по основным параметрам
    const posts: IPagData<Ipost> = await BlogDomain.getAllPosts(
      null,
      null,
      null,
      null
    ) // todo: поиск постов отдельным эндпоинтом
    res.json(posts)
  }

  /**
   * Получение всех постов
   *
   * @param req
   * @param res
   */
  public static async getAllPosts(req: Request, res: Response): Promise<void> {
    // todo: добавь фильтр по основным параметрам
    await check('page').isNumeric().optional().run(req)
    await check('perPage').isNumeric().optional().run(req)
    await check('tag').isString().optional().run(req)
    await check('author').isString().optional().run(req)
    validationResult(req).throw()

    // todo: сделать отдельные типы для page и perPage
    const page = req.query.page
      ? (parseInt(req.query.page as string, 10) as IPostNumber)
      : null
    const perPage = req.query.perPage
      ? (parseInt(req.query.perPage as string, 10) as IPostNumber)
      : null

    const author = req.query.author ? (req.query.author as IPostString) : null
    const tag = req.query.tag ? (req.query.tag as IPostString) : null

    const posts: IPagData<Ipost> = await BlogDomain.getAllPosts(
      author,
      tag,
      page,
      perPage
    )
    res.json(posts)
  }

  /**
   * Получение одного поста по ID
   */
  public static async getOnePost(req: Request, res: Response): Promise<void> {
    const postID: string = req.params.postID
    const post: Ipost = await BlogDomain.getOnePost(postID)
    res.json(post)
  }

  /**
   * Создание 1 ого поста
   *
   * @param req
   * @param res
   */
  public static async postBlog(req: Request, res: Response): Promise<void> {
    await check('header').not().isEmpty().run(req)
    await check('text').not().isEmpty().run(req)
    await check('author').not().isEmpty().run(req)
    validationResult(req).throw()

    // todo: работа с тегами (добавлить, удалить, пропатчить)

    const newPost: Ipost = {
      header: req.body.header ? (req.body.header as IPostString) : null,
      preview: req.body.preview ? (req.body.preview as IPostString) : null,
      text: req.body.text ? (req.body.text as IPostString) : null,
      author: req.body.author ? (req.body.author as IPostString) : null,
      img: req.body.img ? (req.body.img as IPostString) : null,
      tags: req.body.tags ? req.body.tags : null,
      status: PostStatus.Draft,
      isDeleted: false,
    }

    const post: Ipost = await BlogDomain.createOnePost(newPost)
    res.json(post)
  }

  /**
   * Внесение изменений в заданный пост
   *
   * @param req
   * @param res
   */
  public static async patchOnePost(req: Request, res: Response): Promise<void> {
    /*
    await check('header').not().isEmpty().run(req)
    await check('preview').not().isEmpty().run(req)
    await check('text').not().isEmpty().run(req)
    await check('author').not().isEmpty().run(req)
    
    validationResult(req).throw()
    */
    // todo: работа с тегами (добавлить, удалить, пропатчить)

    const targetPost: Ipost = {
      postID: req.params.postID as string,
      header: req.body.header ? (req.body.header as IPostString) : null,
      preview: req.body.preview ? (req.body.preview as IPostString) : null,
      text: req.body.text ? (req.body.text as IPostString) : null,
      author: req.body.author ? (req.body.author as IPostString) : null,
      tags: req.body.tags ? req.body.tags : null,
      status: req.body.status ? req.body.status : null, // todo: helper для защиты статуса от  левых переменных
      isDeleted: req.body.isDeleted ? req.body.isDeleted : null,
    }

    const post: Ipost = await BlogDomain.patchOnePost(targetPost)
    res.json(post)
  }
}
