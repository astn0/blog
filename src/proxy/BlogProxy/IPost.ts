import PostStatus from "./PostStatus";
import IPostString from "./IPostString";

export default interface Ipost{
  postID?: string,
  header: IPostString,
  preview: IPostString,
  text: IPostString,
  author: IPostString,
  status: PostStatus,
  isDeleted?: boolean,
  img?: IPostString,
  tags?: Array<string>|null,
  timeCreate?: Date,
  timeUpdate?: Date
}