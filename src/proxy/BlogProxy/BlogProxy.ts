import Ipost from './IPost'
import BlogModel from '../../models/BlogModel/BlogModel'
import TagsModel from '../../models/TagsModel/TagsModel'
import TagsBlogModel from '../../models/TagsModel/TagsBlogModel'
import ITag from './ITag'


/**
 * Получение и преобразование данных для блога
 */
export default class BlogProxy {
  /**
   * Создаем новый пост и возвращаем его
   * 
   * @param newPost 
   */  
  public async createOnePost (newPost: Ipost): Promise<Ipost> {
    
     
    const oneBlog: any = await BlogModel.create(newPost).catch(e => console.log('error e = ', e)) 

    const tags = newPost.tags as Array<string>
    if (tags) {
      await this._patchTags(oneBlog.id, tags).catch(e => console.log('ошибка тегов')) // todo: пусть возвращает значение
      const postsTags: Array<string> = await this._getTagsForPost(oneBlog.id)
      return this._mapToPost(oneBlog, postsTags)
    } 

    return this._mapToPost(oneBlog, [])
  }

  

  /**
   * Получение всех постов
   */
  public async getAllPosts (): Promise<Array<Ipost>> {
    const resultPostsList: any = await BlogModel.findAll({})
    const posts: Array<Ipost> = []
    for (let resultItem of resultPostsList) {
      const postsTags: Array<string> = await this._getTagsForPost(resultItem.id)
      posts.push(this._mapToPost(resultItem, postsTags))
    }
    return posts
  }

  
  /**
   * Получение одного поста
   */
  public async getOnePost (postID: string): Promise<Ipost> {
    const resultPostsList = await BlogModel.findOne({ 
      where: {
        id: postID
      } 
    })
    const postsTags: Array<string> = await this._getTagsForPost(postID)
    return this._mapToPost(resultPostsList, postsTags)
  }

  
  /**
   * Патчим один пост
   */
  public async patchOnePost (newPost: Ipost): Promise<Ipost> {
    const id = newPost.postID as string

    const tags = newPost.tags as Array<string>
    if (tags) await this._patchTags(id, tags).catch(e => console.log('ошибка тегов'))

    const post: any = { ...newPost }
    for (let item in post) {
      if(!post[item]) delete post[item]
      if(item === 'postID') delete post[item]
      if(item === 'tags') delete post[item]
      if(item === 'timeCreate') delete post[item]
      if(item === 'timeUpdate') delete post[item]
    }
    
    await BlogModel.update({
      ...post
    }, 
    { 
      where: {
        id
      } 
    })

    const resultPostsList = await BlogModel.findOne({ 
      where: {
        id
      } 
    })
    const postsTags: Array<string> = await this._getTagsForPost(id)
    
    return this._mapToPost(resultPostsList, postsTags)
  }

  /**
   * Создаем, добавляем 
   * 
   * @param id 
   * @param tags 
   */
  private async _patchTags (postID: string, tags: Array<string>): Promise<void> { 
    // todo: проверь, что связь с тегом существует, если нет - добавь связь
    if (!Array.isArray(tags)) throw new Error('tags не является массивом')

    let updatedTags = []
    for (let tag of tags) {
      const existingTag = await TagsModel.findOne({ where: { name: tag }})
      if(existingTag) {
        updatedTags.push(this._mapToTag(existingTag))
      } else {
        const newTag = await TagsModel.create({ name: tag })
        updatedTags.push(this._mapToTag(newTag))
      }
    }
    console.log('updatedTags = ', updatedTags)

    for (let tag of updatedTags) {
      const existingTag = await TagsBlogModel.findOne({ where: { tagID: tag.tagID, postID }})
      if(!existingTag) {
        console.log('добавляем тег к посту')
        await TagsBlogModel.create({ tagID: tag.tagID, postID })
      } else {
        console.log('тег к посту добавлен уже')
      }
    }

  }
  
  /**
   * получаем имена тегов(теги то есть) для целевого поста
   * 
   * @param postID 
   */
  private async _getTagsForPost(postID: string): Promise<Array<string>> {
    const tagsPosts: any  = await TagsBlogModel.findAll({ where: { postID }}) 
    if( Array.isArray(tagsPosts)) {
      console.log(' тагс - это массив')
      const tags : Array<string> = []
      for (let tagsPostItem of tagsPosts) {
        let oneTag: any = await TagsModel.findOne({ where: { id: tagsPostItem.tagID }}) 
        if(!oneTag) continue
        tags.push(oneTag.name)
      }
      return tags 
    } else {
      console.log(' тагс = ', tagsPosts)
      return []
    }
    
  }
  /**
   * Преобразование результата из бд в пост
   * 
   * @param data 
   */

  private _mapToPost (data: any, tags: Array<string>): Ipost {
    
    return { 
      postID: data.id,
      header: data.header,
      preview: data.preview,
      text: data.text,
      author: data.author,
      status: data.status,
      img: data.img,
      timeCreate: new Date(data.createdAt),
      timeUpdate: new Date(data.updatedAt),
      tags: tags.length > 0 ? tags : null
    }
  } 

  /**
   * Преобразование результата из БД в тег
   * 
   * @param data 
   */
  private _mapToTag(data: any): ITag {
    return {
      tagID: data.id,
      name: data.name
    }
  }
}
