export default interface ITag {
  tagID: string,
  name: string
}