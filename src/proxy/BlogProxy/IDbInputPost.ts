import IPostString from "./IPostString";
import PostStatus from "./PostStatus";

export default interface IDbInputpost{
  id: string,
  header: string,
  preview: string,
  text: string,
  author: string,
  status: PostStatus,
  isDeleted: boolean,
  img: string,
  // timeCreate: Date,
  // timeUpdate: Date
}