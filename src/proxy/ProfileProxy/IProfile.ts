export default interface IProfile {
    profileID: string
    login: string
    password: string
    email: string
    phone: string
}