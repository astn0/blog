import Router from 'express-promise-router'
import BlogController from '../controllers/BlogController'
const router = Router()

// Работа с блогом
router.route('/v1/blog/posts/').post(BlogController.postBlog)
router.route('/v1/blog/posts/:postID').patch(BlogController.patchOnePost)
router.route('/v1/blog/posts/').get(BlogController.getAllPosts)
router.route('/v1/blog/posts/:postID').get(BlogController.getOnePost)

export default router