import { Sequelize } from 'sequelize'

const SequelizeSingleton = new Sequelize({
    dialect: 'sqlite',
    storage: './database.sqlite'
  })
SequelizeSingleton.sync() 

export default SequelizeSingleton  

