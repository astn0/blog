export default class Pagination {
  /**
   * Пагинация
   *
   * @param paginationParams
   * @param targetArray
   * @private
   */
  public static pagination(
    page: number,
    perPage: number,
    targetArray: Array<any>
  ) {
    if (!page || !perPage) {
      console.log('пагинация не нужна')
      return {
        data: targetArray,
        pagination: {
          hasPrevPage: false,
          hasNextPage: false,
          prevPage: 0,
          nextPage: 0,
          perPage: targetArray.length,
          page: 1,
          totalPages: 1,
        },
      }
    }
    // колличество секций
    let totalReal = targetArray.length / perPage
    let totalPages =
      Math.trunc(totalReal) < totalReal ? Math.trunc(totalReal) + 1 : totalReal
    let hasNextPage = false
    let hasPrevPage = false

    // если ввели номер не существующей страницы, то заменим его на последний существующий
    if (totalPages < page) page = totalPages

    let rightIndex = 0
    for (let i = 0; i !== page; i++) {
      rightIndex += perPage
    }
    let leftIndex = rightIndex - perPage

    if (leftIndex !== 0) hasPrevPage = true
    if (rightIndex < targetArray.length) hasNextPage = true

    let data: Array<any> = targetArray.slice(leftIndex, rightIndex)

    return {
      data,
      pagination: {
        page,
        perPage,
        hasNextPage,
        hasPrevPage,
        prevPage: hasPrevPage ? page - 1 : 0,
        nextPage: hasNextPage ? page + 1 : 0,
        totalPages,
      },
    }
  }
}
