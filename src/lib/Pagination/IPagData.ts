import IPagination from "./IPagination";

export default interface IPagData<T> {
  /**
   * Данные
   */
  data: Array<T>
  /**
   * Пагинация
   */
  pagination: IPagination
}